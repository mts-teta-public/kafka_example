package main

import (
	"context"
	"encoding/binary"
	"fmt"
	"log"

	"github.com/riferrei/srclient"
	"github.com/segmentio/kafka-go"
)

func main() {
	schemaRegistryClient := srclient.CreateSchemaRegistryClient("http://localhost:8081")

	reader := kafka.NewReader(kafka.ReaderConfig{
		Brokers: []string{"127.0.0.1:29092", "127.0.0.1:39092", "127.0.0.1:49092"},
		Topic:   "books",
		GroupID: "my-group",
	})

	for {
		msg, err := reader.FetchMessage(context.Background())
		if err != nil {
			log.Fatal(err)
		}
		schemaID := binary.BigEndian.Uint32(msg.Value[1:5])
		schema, err := schemaRegistryClient.GetSchema(int(schemaID))
		if err != nil {
			log.Fatal(err)
		}
		native, _, err := schema.Codec().NativeFromBinary(msg.Value[5:])
		if err != nil {
			log.Fatal(err)
		}
		for k, v := range native.(map[string]interface{}) {
			fmt.Println("Key: ", k, "\tValue: ", v)
			if k == "author" {
				if v, ok := v.(map[string]interface{}); ok {
					for k1, v1 := range v {
						fmt.Println(k1, "||||", v1)
					}
				}
			}
		}

	}
}
