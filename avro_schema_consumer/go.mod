module avro_consumer

go 1.19

require (
	github.com/riferrei/srclient v0.5.4
	github.com/segmentio/kafka-go v0.4.38
)

require (
	github.com/golang/snappy v0.0.1 // indirect
	github.com/klauspost/compress v1.15.9 // indirect
	github.com/linkedin/goavro/v2 v2.9.7 // indirect
	github.com/pierrec/lz4/v4 v4.1.15 // indirect
	github.com/santhosh-tekuri/jsonschema/v5 v5.0.0 // indirect
	golang.org/x/sync v0.0.0-20201020160332-67f06af15bc9 // indirect
)
