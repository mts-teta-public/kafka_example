from schema_registry.client import SchemaRegistryClient, schema
from schema_registry.serializers import AvroMessageSerializer

from kafka import KafkaProducer

avsc = open("book_v2.avsc", "rb").read().decode("utf-8")

avro_schema = schema.AvroSchema(avsc)
client = SchemaRegistryClient("http://127.0.0.1:8081")
avro_message_serializer = AvroMessageSerializer(client)


# user = {
#     "title": "gRPC: запуск и эксплуатация облачных приложений. Go и Java для Docker и Kubernetes",
#     "author": "Касун Индрасири",
#     "year": 2020
# }

user = {
    "title": "gRPC: запуск и эксплуатация облачных приложений. Go и Java для Docker и Kubernetes",
    "author": {
        "first_name": "Касун",
        "last_name": "Индрасири"
    },
    "year": 2020
}

message_encoded = avro_message_serializer.encode_record_with_schema(
    "book", avro_schema, user)


producer = KafkaProducer(bootstrap_servers='localhost:29092')

result = producer.send('books', message_encoded)
print(result.get(timeout=60))
