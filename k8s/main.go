package main

import (
	"context"
	"io"
	"log"
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/segmentio/kafka-go"
)

const (
	BROKER     = "teta-kafka-kafka-brokers.databases.svc.cluster.local:9092"
	TOPIC_NAME = "team0_demo"
)

func main() {
	ctx := context.Background()
	conn, err := kafka.DialContext(ctx, "tcp", BROKER)
	if err != nil {
		log.Fatal(err)
	}

	err = conn.CreateTopics(kafka.TopicConfig{
		Topic:             TOPIC_NAME,
		NumPartitions:     2,
		ReplicationFactor: 1,
	})
	if err != nil {
		log.Fatal(err)
	}

	r := chi.NewRouter()
	r.Post("/", func(w http.ResponseWriter, r *http.Request) {
		b, err := io.ReadAll(r.Body)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		writer := kafka.NewWriter(kafka.WriterConfig{
			Brokers:      []string{BROKER},
			Topic:        TOPIC_NAME,
			RequiredAcks: 0,
		})

		err = writer.WriteMessages(r.Context(), kafka.Message{Value: b})
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(err.Error())) //nolint:errcheck
			return
		}

		_, err = w.Write([]byte("OK"))
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(err.Error())) //nolint:errcheck
		}
	})
	r.Get("/", func(w http.ResponseWriter, r *http.Request) {
		reader := kafka.NewReader(kafka.ReaderConfig{
			Brokers: []string{BROKER},
			Topic:   TOPIC_NAME,
			GroupID: "my-group",
		})
		msg, err := reader.ReadMessage(r.Context())
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(err.Error())) //nolint:errcheck
			return
		}
		w.WriteHeader(http.StatusOK)
		w.Write(msg.Value) //nolint:errcheck
	})
	http.ListenAndServe(":3000", r) //nolint:errcheck
}
