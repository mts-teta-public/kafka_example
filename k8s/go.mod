module k8s

go 1.19

require (
	github.com/go-chi/chi/v5 v5.0.7
	github.com/segmentio/kafka-go v0.4.38
)

require (
	github.com/klauspost/compress v1.15.9 // indirect
	github.com/pierrec/lz4/v4 v4.1.15 // indirect
)
